use DBI;
use strict;
use FindBin;
use warnings;
use File::Spec;
use Data::Dumper;
use Test::Exception;
use Test::More tests => 12;

require_ok('Geoffrey::Changelog::Database');
use_ok 'Geoffrey::Changelog::Database';

my $s_filepath = '.tmp.sqlite';
unlink $s_filepath;

require Geoffrey::Utils;
my $o_changelog_database = Geoffrey::Changelog::Database->new(
    converter => Geoffrey::Utils::converter_obj_from_name('SQLite'),
    dbh       => DBI->connect("dbi:SQLite:database=$s_filepath"),
);

ok $o_changelog_database->_changlog_entries_table,      'plain sub call test _changlog_entries_table';
ok $o_changelog_database->_changlog_entries_table_name, 'plain sub call test _changlog_entries_table_name';
ok $o_changelog_database->geoffrey_changelogs,          'plain sub call test geoffrey_changelogs';

is $o_changelog_database->file_extension, undef, 'plain sub call test file_extension';
is $o_changelog_database->schema,         undef, 'plain sub call test schema';

throws_ok {
    $o_changelog_database->tpl_main;
}
'Geoffrey::Exception::NotSupportedException::File', 'plain sub call test tpl_main';

throws_ok {
    $o_changelog_database->tpl_sub;
}
'Geoffrey::Exception::NotSupportedException::File', 'plain sub call test tpl_sub';

my $ar_changelog = [
    {
        author  => 'Mario Zieschang',
        id      => '01-01-maz',
        name    => 'Some createative name',
        entries => [ { action => 'sql.add', as => 'SELECT 1;' } ],
    }
];

is(
    Data::Dumper->new( $o_changelog_database->write( q~.~, $ar_changelog ) )->Indent(0)->Terse(1)->Deparse(1)
      ->Sortkeys(1)->Dump,
    Data::Dumper->new(
        [
            'INSERT INTO geoffrey_changelogs ( created_by, filename, geoffrey_version, id) VALUES ( ?, ?, ?, ? )',
            'INSERT INTO geoffrey_changlog_entries ( action, geoffrey_changelog, name, plain_sql) VALUES ( ?, ?, ?, ? )'
        ]
    )->Indent(0)->Terse(1)->Deparse(1)->Sortkeys(1)->Dump,
    'plain sub call test write'
);

is( scalar @{ $o_changelog_database->load }, 1, 'plain sub call test load' );

is(
    Data::Dumper->new( $o_changelog_database->delete('01-01-maz') )->Indent(0)->Terse(1)->Deparse(1)->Dump,
    Data::Dumper->new(
        [
            'DELETE FROM geoffrey_changlog_entries WHERE ( geoffrey_changelog = ? )',
            'DELETE FROM geoffrey_changelogs WHERE ( id = ? )'
        ]
    )->Indent(0)->Terse(1)->Deparse(1)->Dump,
    'delete test with entries'
);

unlink $s_filepath or warn "Could not unlink $s_filepath: $!";
