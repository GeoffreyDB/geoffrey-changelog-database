
use strict;
use FindBin;
use warnings;
use File::Spec;
use Data::Dumper;
use Test::More tests => 7;

use Test::Mock::Geoffrey::DBI;
use Test::Mock::Geoffrey::Converter::SQLite;

require_ok('Geoffrey::Write');
use_ok 'Geoffrey::Write';

my $o_writer = new_ok( 'Geoffrey::Write', [
        changelog_count => 1,
        author          => 'Mario Zieschang',
        converter       => Test::Mock::Geoffrey::Converter::SQLite->new,
        dbh             => Test::Mock::Geoffrey::DBI->new,
        changelog_io    => 'Database'
    ]);

is(
    Data::Dumper->new( $o_writer->triggers )->Indent(0)->Terse(1)->Deparse(1)->Sortkeys(1)->Dump,
    Data::Dumper->new([
        {
            author  => "Mario Zieschang",
            entries => [ { name => "Trigger 1" } ],
            id      => "1-1-maz"
        },
        {
            author  => "Mario Zieschang",
            entries => [ { name => "Trigger 2" } ],
            id      => "1-2-maz"
        }
    ])->Indent(0)->Terse(1)->Deparse(1)->Sortkeys(1)->Dump, 'no file at all'
);

is( $o_writer->inc_changelog_count, 1, 'test inc changelog count' );

is(
    Data::Dumper->new( $o_writer->functions )->Indent(0)->Terse(1)->Deparse(1)->Sortkeys(1)->Dump,
    Data::Dumper->new([
            {
                author  => "Mario Zieschang",
                entries => [ { name => "Function 1" } ],
                id      => "2-1-maz"
            },
            {
                author  => "Mario Zieschang",
                entries => [ { name => "Function 2" } ],
                id      => "2-2-maz"
            }
    ])->Indent(0)->Terse(1)->Deparse(1)->Sortkeys(1)->Dump, 'no file at all'
);

is( $o_writer->inc_changelog_count, 2, 'test inc changelog count' );
