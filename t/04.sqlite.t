use Test::More tests => 4;
use Test::Exception;

use DBI;
use strict;
use FindBin;
use warnings;
use Geoffrey;
use File::Spec;
use Data::Dumper;

my $s_file = '.tmp.sqlite';
unlink $s_file;
my $o_geoffrey = new_ok('Geoffrey',
    [converter_name => 'SQLite', dbh => DBI->connect("dbi:SQLite:database=$s_file"), io_name => 'None']);
ok $o_geoffrey->read(File::Spec->catfile($FindBin::Bin, 'data', 'changelog')), 'Fill database with io "None"';
ok $o_geoffrey->disconnect(), 'Disconnect ok';

$o_geoffrey = new_ok('Geoffrey',
    [converter_name => 'SQLite', dbh => DBI->connect("dbi:SQLite:database=$s_file"), io_name => 'Database']);

#is(
#    Data::Dumper->new( $o_geoffrey->write( q~.~, 'main', 1 ) )->Terse(1)->Deparse(1)->Sortkeys(1)->Dump,
#    Data::Dumper->new(
#        [
#            'INSERT INTO geoffrey_changelogs ( filename,author,geoffrey_version,comment,id ) VALUES (?,?,?,?,?)',
#            'INSERT INTO geoffrey_changlog_entries ( geoffrey_changelogs_id,action,name ) VALUES (?,?,?)',
#            'INSERT INTO geoffrey_changelogs ( filename,author,geoffrey_version,comment,id ) VALUES (?,?,?,?,?)',
#            'INSERT INTO geoffrey_changlog_entries ( geoffrey_changelogs_id,action,name ) VALUES (?,?,?)',
#            'INSERT INTO geoffrey_changelogs ( filename,author,geoffrey_version,comment,id ) VALUES (?,?,?,?,?)',
#            'INSERT INTO geoffrey_changlog_entries ( geoffrey_changelogs_id,action,name ) VALUES (?,?,?)',
#            'INSERT INTO geoffrey_changelogs ( filename,author,geoffrey_version,comment,id ) VALUES (?,?,?,?,?)',
#            'INSERT INTO geoffrey_changlog_entries ( geoffrey_changelogs_id,action,name ) VALUES (?,?,?)',
#            'INSERT INTO geoffrey_changelogs ( filename,author,geoffrey_version,comment,id ) VALUES (?,?,?,?,?)',
#            'INSERT INTO geoffrey_changlog_entries ( geoffrey_changelogs_id,action,name ) VALUES (?,?,?)',
#            'INSERT INTO geoffrey_changelogs ( filename,author,geoffrey_version,comment,id ) VALUES (?,?,?,?,?)',
#            'INSERT INTO geoffrey_changlog_entries ( geoffrey_changelogs_id,action,name ) VALUES (?,?,?)',
#            'INSERT INTO geoffrey_changelogs ( filename,author,geoffrey_version,comment,id ) VALUES (?,?,?,?,?)',
#            'INSERT INTO geoffrey_changlog_entries ( geoffrey_changelogs_id,action,name ) VALUES (?,?,?)',
#            'INSERT INTO geoffrey_changelogs ( filename,author,geoffrey_version,comment,id ) VALUES (?,?,?,?,?)',
#            'INSERT INTO geoffrey_changlog_entries ( geoffrey_changelogs_id,action,name ) VALUES (?,?,?)',
#            'INSERT INTO geoffrey_changelogs ( filename,author,geoffrey_version,comment,id ) VALUES (?,?,?,?,?)',
#            'INSERT INTO geoffrey_changlog_entries ( geoffrey_changelogs_id,action,name ) VALUES (?,?,?)',
#            'INSERT INTO geoffrey_changelogs ( filename,author,geoffrey_version,comment,id ) VALUES (?,?,?,?,?)',
#            'INSERT INTO geoffrey_changlog_entries ( geoffrey_changelogs_id,action,name ) VALUES (?,?,?)',
#            'INSERT INTO geoffrey_changelogs ( filename,author,geoffrey_version,comment,id ) VALUES (?,?,?,?,?)',
#            'INSERT INTO geoffrey_changlog_entries ( geoffrey_changelogs_id,action,name ) VALUES (?,?,?)',
#            'INSERT INTO geoffrey_changelogs ( filename,author,geoffrey_version,comment,id ) VALUES (?,?,?,?,?)',
#            'INSERT INTO geoffrey_changlog_entries ( geoffrey_changelogs_id,action,name ) VALUES (?,?,?)',
#            'INSERT INTO geoffrey_changelogs ( filename,author,geoffrey_version,comment,id ) VALUES (?,?,?,?,?)',
#            'INSERT INTO geoffrey_changlog_entries ( geoffrey_changelogs_id,action,name ) VALUES (?,?,?)',
#        ]
#    )->Terse(1)->Deparse(1)->Sortkeys(1)->Dump,
#    'changeset sub test'
#);

$o_geoffrey->disconnect();

#unlink $s_file or warn "Could not unlink $s_file: $!";
