use Test::More tests => 8;

use strict;
use FindBin;
use warnings;
use Data::Dumper;

use_ok 'DBI';

require_ok('Geoffrey');
use_ok 'Geoffrey';
my $s_file = '.tmp.sqlite';
unlink $s_file;
my $o_geoffrey = new_ok('Geoffrey',
    [converter_name => 'SQLite', dbh => DBI->connect("dbi:SQLite:database=$s_file"), io_name => 'Database',]);

ok($o_geoffrey->isa('Geoffrey'),                'Geoffrey is really Geoffrey');
ok($o_geoffrey->reader->isa('Geoffrey::Read'),  'Check if reader is Geoffrey::Read');
ok($o_geoffrey->writer->isa('Geoffrey::Write'), 'Check if writer is Geoffrey::Write');

my $o_writer = $o_geoffrey->writer;
is($o_writer->author, 'Mario Zieschang', 'author sub test');

#is($o_writer->converter->changelog_table, 'geoffrey_changelogs', 'changelog_table sub test');
#is($o_writer->io_name,                    'Database',            'io_name sub test');
#ok($o_writer->converter->isa('Geoffrey::Converter::SQLite'),      'converter sub test');
#ok($o_writer->dbh->isa('DBI::db'),                                'dbh sub test');
#ok($o_writer->changelog_io->isa('Geoffrey::Changelog::Database'), 'changelog_io sub test');
#ok($o_writer->changeset->isa('Geoffrey::Changeset'),              'changeset sub test');

#is(
#    Data::Dumper->new($o_geoffrey->write(q~.~, 'main', 1))->Terse(1)->Deparse(1)->Sortkeys(1)->Dump,
#    Data::Dumper->new([
#            'INSERT INTO geoffrey_changelogs ( filename,author,geoffrey_version,comment,id ) VALUES (?,?,?,?,?)',
#            'INSERT INTO geoffrey_changelogs ( filename,author,geoffrey_version,comment,id ) VALUES (?,?,?,?,?)'
#        ]
#    )->Terse(1)->Deparse(1)->Sortkeys(1)->Dump,
#    'changeset sub test'
#);

unlink $s_file or warn "Could not unlink $s_file: $!";
