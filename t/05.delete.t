use DBI;
use strict;
use FindBin;
use Geoffrey;
use warnings;
use File::Spec;
use Data::Dumper;
use Test::More tests => 4;

require_ok('Geoffrey::Changelog::Database');
use_ok 'Geoffrey::Changelog::Database';

my $s_file = '.tmp.sqlite';
unlink $s_file;
my $dbh = DBI->connect( "dbi:SQLite:database=$s_file", { PrintError => 0, RaiseError => 1 } );
my $o_geoffrey = new_ok( 'Geoffrey',
    [ converter_name => 'SQLite', dbh => DBI->connect("dbi:SQLite:database=$s_file"), io_name => 'Database', ] );
my $o_changelog_io = $o_geoffrey->changelog_io;

is( $o_changelog_io->delete('01-some-id'), undef, 'simple delete test without entries' );

unlink $s_file;

1;
