FROM perl:5.26

RUN  cpanm -v https://gitlab.com/GeoffreyDB/Geoffrey/-/archive/master/Geoffrey-master.tar.gz
RUN  cpanm -n Test::Pod::Coverage Pod::Coverage::TrustPod Test::Pod Test::Perl::Critic Devel::Cover::Report::Coveralls Test::Exception Test::CheckManifest

COPY cpanfile cpanfile

RUN  cpanm -n --installdeps .
